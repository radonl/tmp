database:
  #Database backend type, can be set to mysql or sqlite
  type: sqlite
  #MySQL database host
  mySQLHost: localhost
  #MySQL connection port number
  mySQLPort: 3306
  #MySQL database name
  mySQLDatabase: db
  #MySQL database user
  mySQLUser: root
  #MySQL user's password
  mySQLPassword: ""
  #MySQL table names
  tableNames:
    auctions: "auctions"
    players: "players"

#Logging settings
log:
  #Define whether transactions will be logged to the console/main server log
  toConsole: true
  #Define whether transactions will be logged to a separate log
  toFile: false
  #Date format of transactions (http://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html)
  formatDate: "yyyy/MM/dd HH:mm:ss"
  #Log message formats
  messageFormats:
    start: "%player% put up %quantity% x %item% for an auction #%id%! Starting price: $%startingprice%, bid increment: $%increment%, duration: %duration%"
    bid: "%player% placed bid of $%amount% on auction #%id% (%quantity% x %item%)."
    end: "%buyer% won auction #%id% (%quantity% x %item%) with a bid of $%price%."
    endNoBids: "Auction #%id% (%quantity% x %item%) ended with no bids."
    cancel: "Auction #%id% (%quantity% x %item%) has been cancelled by the seller."
    cancelAdmin: "Auction #%id% (%quantity% x %item%) has been cancelled by %player%."
  
#Default settings for newly created auctions when not specified
defaultSettings:
  quantity: 1
  price: 100.0
  increment: 10.0
  #Duration in seconds
  duration: 600
  
#Minium settings values for newly created auctions (set to -1 for no limit)
minSettings:
  quantity: -1
  price: -1
  increment: -1
  #Duration in seconds
  duration: -1
  
#Maximum settings values for newly created auctions (set to -1 for no limit), 64 is the maximal supported quantity
maxSettings:
  quantity: 64
  price: -1
  increment: -1
  #Duration in seconds
  duration: -1
  
#Choose the scope of sent messages.
#Valid values are:
#OWNER (send only to the auction owner)
#TAKING_PART (send to auction owner + all players who had bid earlier)
#ALL (send to all online players)
#NONE (the message won't be sent)
messages:
  start: ALL
  bid: TAKING_PART
  end: TAKING_PART
  endNoBids: OWNER
  cancel: ALL
  cancelAdmin: ALL
  
#Choose how many auctions player will be able to run at the same time, permission format is auctionguiplus.limit.NAME eg. auctionguiplus.limit.default
limits:
  #"default" is required
  default: 10
  donator: 50
  
#Choose what gamemodes will prevent players from accessing auctions. Valid values: ADVENTURE, CREATIVE, SPECTATOR, SURVIVAL
disableInGamemodes:
  - ADVENTURE
  - CREATIVE
  - SPECTATOR
  
#Choose in which worlds auctions will be completely disabled
disableInWorlds: []
#Choose if messages shouldn't be broadcasted in worlds with auctions disabled
hideMessagesInDisabledWorlds: false
  
#List of items players won't be to put up for auction. Explained in FAQ #1 here: https://www.spigotmc.org/resources/auctiongui-1-9-support.19015/
bannedItems:
  1:
    material: BEACON
    compareQuantity: false
    compareItemMeta: false
  
#Type of the economy which will handle all transactions. Valid values are VAULT (requires http://dev.bukkit.org/bukkit-plugins/vault/), TOKEN_MANAGER (requires https://www.spigotmc.org/resources/tokenmanager.8610/)
economyType: VAULT
#Choose if auction starting should be handled by GUI
enableStartGui: true
#When start GUI is enabled (with the setting above) and smart start is enabled, the GUI will be opened when any of the settings arguments is missing (eg. /auc start)
enableSmartStartGui: true
#Date format; more info: https://docs.oracle.com/javase/tutorial/i18n/format/simpleDateFormat.html
dateFormat: "dd.MM.yyyy, HH:mm"
#Choose if players will be able to cancel their own auctions
allowCancellingOwnAuctions: true
#Choose if players will be able to bid only fixed amounts (current price + increment) or a custom one using the less/more buttons
fixedBidAmountOnly: false
#Choose if players will be able to bid of any size using the /bid command (when set to true) or only multiplies of the auction increment (when set to false) (takes effect only when fixedBidAmountOnly is disabled)
allowAnyBidAmount: false
#Choose if auctions view will be refresh automatically every X ticks (set below in autoRefreshInterval)
autoRefresh: false
#Interval (in ticks, 20 ticks = 1 second) between refreshing auctions view
#Warning: This can cause log when set to low interval with many players online/auctions running
autoRefreshInterval: 20
#Choose if won/unclaimed items should expire 
autoExpire:
  won:
    #Choose if won items should expire
    expire: false
    #Won items will expire after specified time (in minutes) after not being claimed
    delay: 4320
  unclaimed:
    #Choose if unclaimed (returned/cancelled) items should expire
    expire: true
    #Uncalimed (returned/cancelled) items will expire after specified time (in minutes) after not being claimed
    delay: 4320
  #Auctions will be checked if they're supposed to expire every X minutes specified below (setting this to a low value may result in lag)
  checkInterval: 5
  
#Choose if earnings should be stored in GUI so players can claim it themselves (true) or deposited directly to their accounts (false)
storeEarnings: false
#Minimal remaining auction duration (in milliseconds, 1s = 1000ms) required to bid. Set to -1 to disable
antiSnipeMilliseconds: -1
#Choose if the main menu should be skipped and "All auctions" view should be opened immediately after using the /auction command
skipMainMenu: false